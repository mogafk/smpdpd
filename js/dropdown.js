/* examples on templates/corona_travel/sandbox */

/**
 * Сохраняем все options как свойство. Они будут использоваться 
 * внутри методов  (по jsdoc тут должно быть описание именно конструктора
 * Для описание класса есть @classdesc)
 * @param {(JQuery|string)} [options.$trigger="manual"]  элемент нажатие на который должно пока-
 *                                зывать нам $dropdown
 * @param {(string)} options.template содержит шаблон.
 * @param {JQuery} [options.$container] контейнер.
 * @param {Dropdown~animationShow} options.animationShow
 * @param {Dropdown~position|Coordinates} options.position
 * @param {Dropdown~animationPos} options.animationPos
 * @class
 * @classdesc Создание примитивного окна. Оно может использоваться
 *            как dropdown или нечто вроде модального окна.
 *
 * @event Dropdown#show
 * @event Dropdown#hide
 */

 /*
 * @function animationShow анимирует показ\скрытие
 * @param {JQuery} dropdown контейнер для окна
 * @param {string} state состоянии "hide"/"show"
 */
 /*
 * @function position 
 * @param {JQuery} trigger триггер переданный классу.
 * @returns {Coordinates} 
 */
 /**
 * @typedef Coordinates
 * @type Object
 * @property {number} left отступ слева
 * @property {number} top отступ сверху
 */
 /*
 * @function animationPos анимирует изменение позиции
 * @param {JQuery} dropdown окно
 * @param {Object} newPos новые позиции
 */
function Dropdown(options){
  this.options = options;
}
/*
 * @constructs
*/
Dropdown.prototype.init = function(){
  this.event = $({});

  
  this.template = this.options.template;
  this.$dropdown = $(this.template)

  if(!this.options.$container)
    this.$dropdown.appendTo($(document.body));
  else{
    this.$dropdown.appendTo(this.options.$container);
  }

  if(this.options.$trigger){
    this.$trigger = this.options.$trigger;
    this.$dropdown.attr("trigger-id", this.$trigger.attr("id") || "trigger has no id");
  }else{
    this.$trigger = $();
  }

  if(this.$dropdown.css("display") == "none")
    this.state = "hide";
  else
    this.state = "show";

  this.setPosition();
}
/*
 * Показывает окно.
 * @method
 * @name show
 * @fires Dropdown#show
 */
Dropdown.prototype.show = function(silent){
  if(this.options.animationShow){
    this.options.animationShow(this.$dropdown, this.state);
  }else{
    this.$dropdown.removeClass("hide");
  }

  this.state = "show";

  if(silent == undefined)
    this.event.trigger("show");
} 

/*
 * Скрывает окно
 * @method
 * @name hide
 * @fires Dropdown#hide
 */
Dropdown.prototype.hide = function(silent){
  if(this.options.animationShow){
    this.options.animationShow(this.$dropdown, this.state);
  }else{
    this.$dropdown.addClass("hide");
  }

  this.state = "hide"
  if(silent == undefined)
    this.event.trigger("hide");
} 

/*
 * Вешает обработчики событий 
 * @method
 * @name bind
 */
Dropdown.prototype.bind = function(){
  var self = this;

  //на trigger, если был передан
  if(this.$trigger !== "manual"){
    this.$trigger.on('click', function(e){
        self.show();
      if(self.$trigger.prop("tagName") == "A")
        e.preventDefault();
    })
  }

  //скрытие по клику не на trigger
  if(this.$trigger.length != 0){
    $(document).on("mousedown", function(e){
      if($(e.target).closest(".ct-dropdown").length){
        return;
      }

      if($(e.target).closest(self.$trigger).length > 0){
        return;
      }

      if(self.state == "show")
        self.hide();    
    });
  }


  //Меняет положение при ресайзе.
  //Не сразу. Ожидает 200 милисекунд после предыдущего события
  var rtime,
    timeout = false;
    delta = 200;
  $(window).resize(function() {
      rtime = new Date();
      if (timeout === false) {
          timeout = true;
          setTimeout(resizeend, delta);
      }
  });

  function resizeend() {
      if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
      } else {
          timeout = false;
            self.setPosition();
      }               
  }
}
/*
 * Устанавливает позицию 
 * @method
 * @name setPosition
 * @param {Coordinates} [newPos] если передан устанавливает новую позицию
 * @param {boolean} [fl_hold=true] в случае если у нас передан обьект,
 *                 то возможно мы не хотим, чтобы он запоминал эти статические
 *                 координаты. В таком случае можно передать false
 */
Dropdown.prototype.setPosition = function(newPos, fl_hold){
  var self = this,
   offsetTrigger = this.$trigger.length >0 ? this.$trigger.offset() : {left:0, top:0};


  if(newPos){
    this.holdPos = newPos;
      if($.type(newPos) == 'function'){
        // console.log(newPos);
        this.options.position = newPos;
        this.setPosition();
        return;
      }else{
        var fl_hold = this._fl_hold = fl_hold != undefined ? fl_hold : true;
        if(fl_hold){
          this.options.position = function(){
            return newPos;
          }
          this.setPosition();
          return;

        }else
          this.pos = newPos;
      }
  }else{
    if(this.options.position){
      this.pos = this.options.position(this.$trigger);
    }else{
      this.pos = {
        top: offsetTrigger.top + this.$trigger.outerHeight(),
        left: offsetTrigger.left
      }
    }
  }


  if(this.options.animationPos){
    this.options.animationPos(this.$dropdown, this.pos);
  }else{
    this.$dropdown.css({
      left: this.pos.left,
      top: this.pos.top
    });
  };
}
//Dropdown.prototype.method = function(){} 


/**
 * Сохраняем все options как свойство. Они будут использоваться 
 * внутри методов
 * @class
 * @classdesc Создание окна со скролом
  *
 * @extends Dropdown
 */
function ScrolledDropdown(options){
  this.options = options;
}
ScrolledDropdown.prototype = Object.create(Dropdown.prototype);
ScrolledDropdown.prototype.super = Dropdown.prototype;
ScrolledDropdown.constructor = ScrolledDropdown;

/*
 * @constructs
*/
ScrolledDropdown.prototype.init = function(){
  this.super.init.call(this);

  this.scrollInit();
}

/*
 * Меняет размер палочки в скроле взависимости от размера
 * внутри scrolled элемента. Делает это с анимацией.
 * Минимальный размеро 25px
 * @method
 * @name setSize
 */
ScrolledDropdown.prototype.setSize = function(){
  var koeff = (this.$scroll.height()/this.$scrolled[0].scrollHeight)*(this.$scroll.height()/2);

  if(koeff < 25){
    koeff = 25;
  }

  this.$scroll.find(".ct-scroll-bg").animate({
    "padding-top": koeff/2,
    "padding-bottom": koeff/2
  }, 1000);
  this.$scrollBody.find(".ui-slider-handle").animate({
    height: koeff,
    "margin-bottom": -koeff/2,
    background: "#c666"
  }, 1000, function(){
  });

}

/*
 * Устанавливает соотвествие едениц скролла и scrolled контента
 * В случае, если scrolled контент динамический и изменяется
 * необходимо вызывать этот метод.
 * В случае, если контент scrolled элемента в него помещается целиком
 * скрывает скролл.
 * @method
 * @name recalcScroll
 */
ScrolledDropdown.prototype.recalcScroll = function() {
  // Нельзя получить некоторые высоты, если блок скрыт. Поэтому покажем
  // его сначала. Так же надо учитывать дефолтное состояние: если окно было
  // уже показано, то ненадо его скрывать и показывать заново.
  if(this.state == "hide")
    var start_state = "hide";
  if(start_state == "hide")
    this.show();


  //вмещается ли контент в scrolled?
  if(this.$scrolled[0].scrollHeight - this.$scrolled.height() < 20){
    this.$scroll.hide();
  }else{
    this.$scroll.show();
  }

  //было ли предыдущее значение?
  //Если да, то на основе его высчитать новое.
  //UX сценарий:
  //1) пользователь проскролил вниз
  //2) изменилось содержимое
  //3) нас не должно перекидывать в самый верх
  var value;
  if(this.scroll_iu !== undefined){
    value = this.$scrollBody.slider("value")*(this.$scrolled[0].scrollHeight-this.$scrolled.height())/this.oldMaxSlider;
  }

  this.$scrollBody.slider({
    orientation: "vertical",
    min: 0,
    max: this.$scrolled[0].scrollHeight-this.$scrolled.height()
  });
  this.oldMaxSlider = this.$scrolled[0].scrollHeight-this.$scrolled.height();
  // this.$scrollBody.slider("value", this.$scrollBody.slider("option", "max")); 
  this.$scrollBody.slider("value", value || this.$scrollBody.slider("option", "max")); 
  this.scroll_iu = this.$scrollBody.slider("instance");

  this.setSize();

  if(start_state == "hide")
    this.hide();  
};
/*
 * Используется для создания структуры внутри контейнера
 * для скролла. Посокльку используется slider от jquery-ui его не так
 * просто закастомить. Структура позволяет легко менять цвет и размер
 * Заметьте, что это приватный метод
 * @method
 * @private
 * @name scrollInit
 */
ScrolledDropdown.prototype.scrollInit = function(){
  console.log("scrollInit " + this.$trigger.attr("id"))

  this.$scrolled = this.$dropdown.find("[data-role='scrolled']");
  this.$scroll = this.$dropdown.find(".ct-scroll");
  $("<div class='ct-scroll-bg'><div class='ct-scroll-body'></div></div>").appendTo(this.$scroll);
  this.$scrollBody = this.$scroll.find(".ct-scroll-body");

  this.recalcScroll();
};

/*
 * Вешает обработчики событий.
 * @method
 * @name bind
 */
ScrolledDropdown.prototype.bind = function() {
  this.super.bind.call(this);

  var self = this;

  this.$scrollBody.on("slide", function(event, ui){
    self.$scrolled.scrollTop(self.$scrollBody.slider("option", "max") - ui.value);
  });

  this.$scrolled.on("mousewheel", function(e){
    if(event.deltaY < 0)
      self.$scrolled.scrollTop(self.$scrolled.scrollTop()-10);
    else
      self.$scrolled.scrollTop(self.$scrolled.scrollTop()+10);

    self.$scrollBody.slider("value", self.$scrollBody.slider("option", "max") - self.$scrolled.scrollTop());   

    e.preventDefault();
  })
};

/*
 * Фабрика, которая взависимости от аргумента вызывает определенный конструктор
 * а так же берет содержимое тектовое из шаблона. Плюс вызывает методы
 * инициализации и биндинга
 * @function 
 * @param {Object} optionsArg аналогично для Dropdown за исключением того
 *                            что подготавливает переменную tempate
 *                            гарантируя, что это будет строка
 * @returns {Dropdown|ScrolledDropdown}
 */
function fabricDropdown(optionsArg){
  var options = optionsArg;
  if($.type(optionsArg.template) !== "string"){
    options.template = $(optionsArg.template).html();
  }
  var instance;

  if(options.scroll == true){
    instance = new ScrolledDropdown(options);
  }else{
    instance = new Dropdown(options);
  }

  instance.init();
  instance.bind();
  return instance;
}